document.addEventListener('DOMContentLoaded', () => {
  const $go = document.querySelector('#go');
  const $overlay = document.querySelector('#overlay');
  const $world = document.querySelector('#world');
  const $playground = document.querySelector('#playground');
  const $layer = document.querySelector('#layer');
  const $aim = document.querySelector('#aim');
  const $vir = document.querySelector('#vir');
  let clientX = document.querySelector('#clientx');
  let clientY = document.querySelector('#clienty');
  let playgroundRect = {
    width: 600,
    height: 391,
  };
  $playground.style.width = playgroundRect.width + "px";
  $playground.style.height = playgroundRect.height + "px";

  class Game {
    constructor() {
      this.reset(this);
      this.intervalTime = 2000;
      this.disappearTime = 1000;
    }
    start() {
      this.interval = setInterval(() => {
        this.show(this);
        setTimeout(this.hide, this.disappearTime, this);
      }, this.intervalTime);
    }
    stop(self) {
      clearInterval(self.interval);
    }
    reset(self) {
      self.player = new Player();
      $overlay.style.width = 0;
      $go.style.display = 'none';
      self.updateInfo(self);
    }
    eliminatedIncrement(self) {
      self.player.eliminatedIncrement();
    }
    static randomInt(min, max) {
      return min + Math.floor((max - min) * Math.random());
    }
    updateInfo(self) {
      document.querySelector('#score').innerText = self.player.score;
      document.querySelector('#eliminated').innerText = self.player.eliminated;
      document.querySelector('#missed').innerText = self.player.missed;
    }
    show(self) {
      const vir = document.querySelector('#vir');
      let x = Game.randomInt(0, 520);
      let y = Game.randomInt(0, 310);
      vir.style.left = x + "px";
      vir.style.top = y + "px";
      vir.style.display = "block";
      self.virusShow = true;
    }
    hide(self) {
      if (!self.virusShow) {
        return;
      }
      self.virusShow = false;
      const vir = document.querySelector('#vir');
      vir.style.display = "none";
      self.player.missedIncrement();
      self.updateInfo(self);
      let missed = self.player.missed;
      let redArea = missed * 60;
      $overlay.style.width = redArea + "px";
      if (redArea >= $world.clientWidth) {
        self.stop(self);
        $go.style.display = "block";
      }
    }
  }

  class Player {
    constructor() {
      this.score = 0;
      this.eliminated = 0;
      this.missed = 0;
    }
    missedIncrement() {
      this.missed++;
      this.score--;
    }
    eliminatedIncrement() {
      this.eliminated++;
      this.score++;
    }
  }

  let game = new Game();
  game.start();

  const $restart = document.querySelector('#restart');
  const $newGame = document.querySelector('#new-game');
  $restart.addEventListener('click', () => {
    game.stop(game);
    game.reset(game);
  });
  $newGame.addEventListener('click', () => {
    game.stop(game);
    game.reset(game);
    game.start();
  });

  let mousemoveEvent = (e) => {
    clientX.innerText = `${e.layerX}`;
    clientY.innerText = `${e.layerY}`;
    $aim.style.left = (e.layerX - 25) + "px";
    $aim.style.top = (e.layerY - 25) + "px";
  };
  let aimClickEvent = (e) => {
    if ($vir.offsetLeft + 80 >= e.layerX
      && $vir.offsetLeft <= e.layerX
      && $vir.offsetTop + 80 >= e.layerY
      && $vir.offsetTop <= e.layerY
    ) {
      game.virusShow = false;
      $vir.style.display = "none";
      game.eliminatedIncrement(game);
      game.updateInfo(game);
    }
  };
  let keyPressEvent = (e) => {
    switch (e.key) {
      case 'a':
        $aim.style.left = ($aim.offsetLeft - 15) + "px";
        break;
      case 'd':
        $aim.style.left = ($aim.offsetLeft + 15) + "px";
        break;
      case 'w':
        $aim.style.top = ($aim.offsetTop - 15) + "px";
        break;
      case 's':
        $aim.style.top = ($aim.offsetTop + 15) + "px";
        break;
      case 'Enter':
        if ($vir.offsetLeft + 80 >= $aim.offsetLeft
          && $vir.offsetLeft <= $aim.offsetLeft
          && $vir.offsetTop + 80 >= $aim.offsetTop
          && $vir.offsetTop <= $aim.offsetTop
        ) {
          game.virusShow = false;
          $vir.style.display = "none";
          game.eliminatedIncrement(game);
          game.updateInfo(game);
        }
        break;
    }
  };
  let $inputDevice = document.querySelectorAll('input[name="inputDevice"]');
  $inputDevice.forEach(radioButton => {
    radioButton.addEventListener('change', (e) => {
      game.stop(game);
      game.reset(game);
      if (e.target.id === 'keyboard') {
        $layer.removeEventListener('mousemove', mousemoveEvent);
        $layer.removeEventListener('click', aimClickEvent);
        document.body.addEventListener('keypress', keyPressEvent);
        game.intervalTime = 4000;
        game.disappearTime = 3000;
      } else if (e.target.id === 'mouse') {
        $layer.removeEventListener('keypress', keyPressEvent);
        $layer.addEventListener('mousemove', mousemoveEvent);
        $layer.addEventListener('click', aimClickEvent);
        game.intervalTime = 2000;
        game.disappearTime = 1000;
      }
      game.start();
    });
  });
  let $keyboard = document.querySelector('#keyboard');
  let $mouse = document.querySelector('#mouse');
  $keyboard.click();
  $mouse.click();
});
